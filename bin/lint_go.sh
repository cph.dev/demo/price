#!/usr/bin/env bash

trap 'exit' SIGINT

limit=$(ulimit -n)
[[ $limit -lt 4096 ]] && ulimit -n 4096

while true; do
	find . -type f -name '*.go' | entr -c -d sh -c "docker run --rm -v $(pwd):/app -w /app golangci/golangci-lint:latest-alpine golangci-lint run $@ \
		&& echo '🟩 Great victory' \
		|| echo '🟥 Big disgrace'"
done

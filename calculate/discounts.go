/*
Copyright 2023 The Authors (See AUTHORS file)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package calculate

import "github.com/shopspring/decimal"

// Define discount options
var (
	seniorDiscountPercent            = decimal.NewFromInt(5)
	premiumMembershipDiscountPercent = decimal.NewFromInt(10)
)

// WithDiscount calculate the discounted price for
// seniors and customers with Premium Membership.
func WithDiscount(
	basePrice decimal.Decimal,
	seniorDiscount bool,
	premiumMembership bool,
) decimal.Decimal {
	finalPrice := basePrice
	oneHundred := decimal.NewFromInt(100)
	if seniorDiscount {
		adjustTo := oneHundred
		adjustTo = adjustTo.Sub(seniorDiscountPercent)
		finalPrice = finalPrice.Mul(adjustTo).Div(oneHundred)
	}
	if premiumMembership {
		adjustTo := oneHundred
		adjustTo = adjustTo.Sub(premiumMembershipDiscountPercent)
		finalPrice = finalPrice.Mul(adjustTo).Div(oneHundred)
	}
	return finalPrice.Truncate(2)
}

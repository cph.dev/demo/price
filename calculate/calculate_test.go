/*
Copyright 2023 The Authors (See AUTHORS file)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package calculate_test

import (
	"testing"

	"github.com/shopspring/decimal"
	"go.cph.dev/price/calculate"
)

func TestPriceWithDiscount(t *testing.T) {
	testCases := []struct {
		name              string
		basePrice         decimal.Decimal
		senior            bool
		premiumMembership bool
		want              decimal.Decimal
	}{
		{
			name:      "No discount",
			basePrice: decimal.NewFromInt(1000),
			want:      decimal.NewFromInt(1000),
		},
		{
			name:      "Senior discount",
			basePrice: decimal.NewFromInt(1000),
			senior:    true, // 5% discount
			want:      decimal.NewFromInt(950),
		},
		{
			name:              "Premium member discount",
			basePrice:         decimal.NewFromInt(1000),
			premiumMembership: true, // 10% discount
			want:              decimal.NewFromInt(900),
		},
		{
			name:              "Precision is two digits (rounded down)",
			basePrice:         decimal.NewFromFloat(899.95),
			premiumMembership: true, // 10% discount
			want:              decimal.NewFromFloat(809.95),
		},
		{
			name:              "Both discounts",
			basePrice:         decimal.NewFromFloat(1000),
			premiumMembership: true, // 10% discount
			senior:            true,
			want:              decimal.NewFromFloat(855),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := calculate.WithDiscount(tc.basePrice, tc.senior, tc.premiumMembership)
			if !tc.want.Equal(got) {
				t.Errorf("Wrong discounted price. Want %q, got %q", tc.want, got)
			}
		})
	}
}
